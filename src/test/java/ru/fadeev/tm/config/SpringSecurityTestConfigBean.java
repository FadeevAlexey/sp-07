package ru.fadeev.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Service;
import ru.fadeev.tm.dto.UserDTO;
import ru.fadeev.tm.entity.Role;
import ru.fadeev.tm.entity.SessionUser;
import ru.fadeev.tm.enumerated.RoleType;

import java.util.ArrayList;
import java.util.List;
import static ru.fadeev.tm.util.TestUtil.*;

@TestConfiguration
@Service("userDetailsTest")
public class SpringSecurityTestConfigBean implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        @NotNull ru.fadeev.tm.entity.User user = user();
        if (username.equals("Admin")) user = admin();
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        builder = org.springframework.security.core.userdetails.User.withUsername(username);
        builder.password(user.getPasswordHash());
        final List<Role> userRoles = user.getRoles();
        final List<String> roles = new ArrayList<>();
        for (Role role : userRoles) roles.add(role.getRole().toString());
        builder.roles(roles.toArray(new String[]{}));
        UserDetails details = builder.build();

        org.springframework.security.core.userdetails.User result = null;
        result = (org.springframework.security.core.userdetails.User) details;

        final SessionUser sessionUser = new SessionUser(result);
        sessionUser.setUserId(user.getId());
        return sessionUser;
    }

}