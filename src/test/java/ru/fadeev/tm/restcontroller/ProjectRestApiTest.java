package ru.fadeev.tm.restcontroller;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;

import org.junit.Test;
import ru.fadeev.tm.dto.ProjectDTO;

public class ProjectRestApiTest extends AbstractRestApiTest {

    @Test
    public void persistProjectTest() {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("testProject");
        projectClient.createProject(userProfile, project);
        @NotNull final ProjectDTO result = projectClient.getProject(userProfile, project.getId());
        Assert.assertNotNull(result);
        Assert.assertEquals("testProject", result.getName());
        projectClient.deleteProject(userProfile, project.getId());
    }


    @Test
    public void findAllProjectTest() {
        final int startTest = projectClient.findAll(userProfile).size();
        @NotNull ProjectDTO project = new ProjectDTO();
        @NotNull ProjectDTO project2 = new ProjectDTO();
        @NotNull ProjectDTO project3 = new ProjectDTO();
        projectClient.createProject(userProfile, project);
        projectClient.createProject(userProfile, project2);
        projectClient.createProject(userProfile, project3);
        final int finishTest = projectClient.findAll(userProfile).size();
        Assert.assertEquals(startTest, finishTest - 3);
        Assert.assertNotNull(projectClient.getProject(userProfile, project.getId()));
        @Nullable ProjectDTO projectDTO = null;
        try {
            projectDTO = projectClient.getProject(adminProfile, project.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
        projectClient.deleteProject(userProfile, project.getId());
        projectClient.deleteProject(userProfile, project2.getId());
        projectClient.deleteProject(userProfile, project3.getId());
    }

    @Test
    public void getOneProjectTest() {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("test");
        projectClient.createProject(userProfile, project);
        @NotNull final ProjectDTO testProject = projectClient.getProject(userProfile, project.getId());
        Assert.assertEquals(project.getName(), testProject.getName());
        @Nullable ProjectDTO projectDTO = null;
        try {
            projectDTO = projectClient.getProject(adminProfile, project.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
        Assert.assertNull(projectDTO);
        projectClient.deleteProject(userProfile, project.getId());
    }

    @Test
    public void mergeProjectTest() {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("test");
        projectClient.createProject(userProfile, project);
        @NotNull ProjectDTO testProject = projectClient.getProject(userProfile, project.getId());
        testProject.setDescription("test description");
        projectClient.updateProject(userProfile, testProject);
        testProject = projectClient.getProject(userProfile, testProject.getId());
        Assert.assertEquals("test description", testProject.getDescription());
        @Nullable ProjectDTO projectDTO = null;
        try {
            projectDTO = projectClient.getProject(adminProfile, project.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
        Assert.assertNull(projectDTO);
        projectClient.deleteProject(userProfile, project.getId());
    }

    @Test
    public void removeProjectTest() {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("test");
        projectClient.createProject(userProfile, project);
        Assert.assertEquals("test", projectClient.getProject(userProfile, project.getId()).getName());
        projectClient.deleteProject(userProfile, project.getId());
        try {
            projectClient.getProject(userProfile, project.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
    }

}
