package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.fadeev.tm.entity.Task;

public interface ITaskRepository extends JpaRepository<Task, String>, JpaSpecificationExecutor<Task> {

    void removeByUser_IdAndId(@NotNull String userId, @NotNull String id);

    void removeAllByUser_Id(@NotNull String userId);

}