package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.User;

import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;

public class ProjectServiceTest extends AbstractServiceTest {

    @Autowired
    private IProjectService projectService;

    @Test
    public void persistProjectTest() {
        @NotNull Project project = new Project();
        project.setName("testProject");
        projectService.persist(project);
        assertNotNull(projectService.findOne(project.getId()));
        projectService.remove(project.getId());
    }

    @Test
    public void removeProjectTest() {
        @Nullable Project project = new Project();
        project.setName("testProject");
        projectService.persist(project);
        projectService.remove(project.getId());
        project = projectService.findOne(project.getId());
        assertNull(project);
    }

    @Test
    public void removeProjectTestUserId() {
        @Nullable User user = userService.findUserByLogin("User");
        @Nullable User admin = userService.findUserByLogin("Admin");
        @Nullable Project project = new Project();
        project.setName("testProject");
        project.setUser(user);
        projectService.persist(project);
        projectService.remove(admin.getId(), project.getId());
        project = projectService.findOne(project.getId());
        assertEquals("testProject", project.getName());
        projectService.remove(user.getId(), project.getId());
        assertNull(projectService.findOne(project.getId()));
    }

    @Test
    public void findOneProjectTestUserId() {
        @Nullable User user = userService.findUserByLogin("User");
        @Nullable User admin = userService.findUserByLogin("Admin");
        @Nullable Project project = new Project();
        project.setName("testProject");
        project.setUser(user);
        projectService.persist(project);
        assertNull(projectService.findOne(admin.getId(), project.getId()));
        project = projectService.findOne(user.getId(), project.getId());
        assertEquals("testProject", project.getName());
        projectService.remove(project.getId());
    }

    @Test
    public void findOneProjectTest() {
        @Nullable Project project = new Project();
        project.setName("testProject");
        projectService.persist(project);
        project = projectService.findOne(project.getId());
        assertEquals("testProject", project.getName());
        projectService.remove(project.getId());
        assertNull(projectService.findOne(null));
        assertNull(projectService.findOne(UUID.randomUUID().toString()));
    }

    @Test
    public void findAllProjectTest() {
        final int projectCount = projectService.findAll().size();
        @NotNull Project project = new Project();
        @NotNull Project project2 = new Project();
        @NotNull Project project3 = new Project();
        projectService.persist(project);
        projectService.persist(project2);
        projectService.persist(project3);
        final int finalCount = projectService.findAll().size();
        Assert.assertEquals(projectCount, finalCount - 3);
        projectService.remove(project.getId());
        projectService.remove(project2.getId());
        projectService.remove(project3.getId());
    }

    @Test
    public void findAllProjectUserId() {
        @Nullable User user = userService.findUserByLogin("User");
        @Nullable User admin = userService.findUserByLogin("Admin");
        final int projectCount = projectService.findAll(user.getId()).size();
        @NotNull Project project = new Project();
        project.setUser(user);
        @NotNull Project project2 = new Project();
        project2.setUser(user);
        @NotNull Project project3 = new Project();
        project3.setUser(user);
        projectService.persist(project);
        projectService.persist(project2);
        projectService.persist(project3);
        final int finalCount = projectService.findAll(user.getId()).size();
        Assert.assertEquals(projectCount, finalCount - 3);
        @Nullable Project adminTest = projectService.findOne(admin.getId(), project.getId());
        assertNull(adminTest);
        projectService.remove(project.getId());
        projectService.remove(project2.getId());
        projectService.remove(project3.getId());
    }

    @Test
    public void convertToProjectTest() {
        @Nullable User user = userService.findUserByLogin("User");
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId(user.getId());
        projectDTO.setFinishDate(new Date(System.currentTimeMillis()));
        projectDTO.setStartDate(new Date(System.currentTimeMillis()));
        projectDTO.setDescription("testDescription");
        Project project = projectService.convertToProject(projectDTO);
        assertEquals(projectDTO.getId(), project.getId());
        assertEquals(projectDTO.getName(), project.getName());
        assertEquals(projectDTO.getDescription(), project.getDescription());
        assertEquals(projectDTO.getFinishDate(), project.getFinishDate());
        assertEquals(projectDTO.getUserId(), project.getUser().getId());
        assertEquals(projectDTO.getStatus(), project.getStatus());
    }

    @Test
    public void mergeProjectTest() {
        Project project = new Project();
        project.setName("Igor");
        projectService.persist(project);
        assertEquals("Igor", projectService.findOne(project.getId()).getName());
        project.setName("Vitalik");
        projectService.merge(project);
        assertEquals("Vitalik", projectService.findOne(project.getId()).getName());
        projectService.remove(project.getId());
    }

    @Test
    public void removeAllProjectTest() {
        @Nullable User user = userService.findUserByLogin("User");
        @NotNull Project project = new Project();
        project.setUser(user);
        projectService.persist(project);
        @NotNull Project project2 = new Project();
        project2.setUser(user);
        projectService.persist(project2);
        assertTrue(projectService.findAll(user.getId()).size() > 1);
        projectService.removeAll(user.getId());
        assertTrue(projectService.findAll(user.getId()).isEmpty());
    }

}