package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.specification.Specifications;

import java.util.UUID;

import static org.junit.Assert.*;

public class UserRepositoryTest extends AbstractRepository {

    @Test
    public void saveUserTest() {
        @Nullable User user = new User();
        user.setLogin(UUID.randomUUID().toString());
        userRepository.save(user);
        assertNotNull(userRepository.findById(user.getId()));
        userRepository.deleteById(user.getId());
    }

    @Test
    public void deleteByIdUserTest() {
        @Nullable User user = new User();
        user.setLogin("test");
        userRepository.save(user);
        userRepository.deleteById(user.getId());
        user = userRepository.findById(user.getId()).orElse(null);
        assertNull(user);
    }

    @Test
    @Transactional
    public void getOneUserTest() {
        @Nullable User user = userRepository.findOne(Specifications.findUserByLogin("User")).orElse(null);
        assertEquals("User", user.getLogin());
        assertNull(userRepository.findOne(Specifications.findUserByLogin(UUID.randomUUID().toString())).orElse(null));
    }

    @Test
    @Transactional
    public void findAllUsers() {
        final int usersCount = userRepository.findAll().size();
        @NotNull User user = new User();
        user.setLogin(UUID.randomUUID().toString());
        @NotNull User user2 = new User();
        user2.setLogin(UUID.randomUUID().toString());
        @NotNull User user3 = new User();
        user3.setLogin(UUID.randomUUID().toString());
        userRepository.saveAndFlush(user);
        userRepository.saveAndFlush(user2);
        userRepository.saveAndFlush(user3);
        final int finalCount = userRepository.findAll().size();
        Assert.assertEquals(usersCount, finalCount - 3);
        userRepository.deleteById(user.getId());
        userRepository.deleteById(user2.getId());
        userRepository.deleteById(user3.getId());
    }

    @Test
    public void mergeUserTest() {
        User user = new User();
        user.setLogin("Igor");
        userRepository.save(user);
        assertEquals("Igor", userRepository.findById(user.getId()).orElse(null).getLogin());
        user.setLogin("Vitalik");
        userRepository.save(user);
        assertEquals("Vitalik", userRepository.findById(user.getId()).orElse(null).getLogin());
        userRepository.deleteById(user.getId());
    }

    @Test
    public void IsLoginExistTest() {
        assertTrue(userRepository.findAll(Specifications.isLoginExist("Admin")).size() > 0);
        assertTrue(userRepository.findAll(Specifications.isLoginExist("User")).size() > 0);
        assertFalse(userRepository.findAll(Specifications.isLoginExist(UUID.randomUUID().toString())).size() > 0);
    }

    @Test
    public void findUserByLogin() {
        assertEquals("User", userRepository.findOne(Specifications.findUserByLogin("User")).orElse(null).getLogin());
        assertEquals("Admin", userRepository.findOne(Specifications.findUserByLogin("Admin")).orElse(null).getLogin());
        assertNull(userRepository.findOne(Specifications.findUserByLogin(UUID.randomUUID().toString())).orElse(null));
    }

}