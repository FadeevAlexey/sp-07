package ru.fadeev.tm.restcontroller;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Base64Utils;
import ru.fadeev.tm.client.ProjectClient;
import ru.fadeev.tm.client.TaskClient;
import ru.fadeev.tm.client.UserClient;
import ru.fadeev.tm.service.UserService;

import static ru.fadeev.tm.util.TestUtil.admin;
import static ru.fadeev.tm.util.TestUtil.user;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractRestApiTest {

    @LocalServerPort
    private int port;

    @Autowired
    private UserService userService;

    protected ProjectClient projectClient;

    protected TaskClient taskClient;

    protected UserClient userClient;

    private String serverUrl = "http://localhost:%s/";

    @Before
    public void setUp() {
        userService.persist(admin());
        userService.persist(user());
        projectClient = ProjectClient.client(String.format(serverUrl, port));
        taskClient = TaskClient.client(String.format(serverUrl, port));
        userClient = UserClient.client(String.format(serverUrl, port));
    }

    protected String auth(@NotNull final String username, final String password) {
        byte[] encodedBytes = Base64Utils.encode((username + ":" + password).getBytes());
        return "Basic " + new String(encodedBytes);
    }

    @NotNull
    final protected String userProfile = auth("User", "user");

    @NotNull
    final protected String adminProfile = auth("Admin", "admin");

}
