<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/header-search.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/str.css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>
<body>
<header class="header-search">
    <div class="header-limiter">
        <h1><a href="${pageContext.request.contextPath}/">TASK<span>MANAGER</span></a></h1>
        <nav>
            <a href="${pageContext.request.contextPath}/">Home</a>
            <a href="${pageContext.request.contextPath}/project">Projects</a>
            <a href="#" class="selected">Tasks</a>
            <sec:authorize access="hasRole('ADMINISTRATOR')">
                <a href="${pageContext.request.contextPath}/admin/users">Users</a>
            </sec:authorize>
        </nav>
        <form method="get" class="selected">
            <a href="${pageContext.request.contextPath}/user/view">${login}</a>
        </form>
    </div>
</header>
<div id="demo">
    <h1>Task Management</h1>
    <div class="table-responsive-vertical shadow-z-1">
        <table id="table" class="table table-hover table-mc-light-blue">
            <thead>
            <tr>
                <th>№</th>
                <th>Project</th>
                <th>Task</th>
                <th>Description</th>
                <th>Status</th>
                <th>View</th>
                <th>Edit</th>
                <th>Remove</th>
            </tr>
            </thead>
            <c:if test="${not empty tasks}">
                <c:forEach var="listValue" items="${tasks}" varStatus="loop">
                    <tbody>
                    <tr>
                        <td data-title="ID">${loop.count}.</td>
                        <td data-title="Name">${listValue.projectName}</td>
                        <td data-title="Name">${listValue.name}</td>
                        <td data-title="Name">${listValue.description}</td>
                        <td data-title="Status">${listValue.status.displayName}</td>
                        <td data-title="Link"><a href="task/view/${listValue.id}">View</a></td>
                        <td data-title="Link"><a href="task/edit/${listValue.id}">Edit</a></td>
                        <td data-title="Link"><a href="task/remove/${listValue.id}">Delete</a>
                        </td>
                    </tr>
                    </tbody>
                </c:forEach>
            </c:if>
        </table>
    </div>
    <button class="btn" type="button" onclick="document.location='task/create'"><span>Create task</span></button>
    <button class="btn orange" type="button" onClick="window.location.reload();"><span>Refresh</span></button>
</div>
</body>
</html>