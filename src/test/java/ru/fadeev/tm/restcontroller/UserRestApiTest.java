package ru.fadeev.tm.restcontroller;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.fadeev.tm.dto.UserDTO;

import java.util.List;
import java.util.UUID;

public class UserRestApiTest extends AbstractRestApiTest {

    @Test
    public void persistUserTest() {
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(UUID.randomUUID().toString());
        userClient.createUser(userDTO);
        @NotNull final UserDTO userResult = userClient.getUserAdmin(adminProfile, userDTO.getId());
        Assert.assertEquals(userDTO.getLogin(), userResult.getLogin());
        userClient.deleteUserAdmin(adminProfile, userDTO.getId());
    }

    @Test
    public void findAllUserTest() {
        final int startTest = userClient.findAll(adminProfile).size();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(UUID.randomUUID().toString());
        userClient.createUser(user);
        final int finishTest = userClient.findAll(adminProfile).size();
        Assert.assertEquals(startTest, finishTest - 1);
        Assert.assertNotNull(userClient.getUserAdmin(adminProfile, user.getId()));
        @Nullable List<UserDTO> userDTOList = null;
        try {
            userDTOList = userClient.findAll(userProfile);
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("403"));
        }
        Assert.assertNull(userDTOList);
        userClient.deleteUserAdmin(adminProfile, user.getId());
    }

    @Test
    public void getOneUserTestAdmin() {
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(UUID.randomUUID().toString());
        userClient.createUser(user);
        @NotNull final UserDTO testUser = userClient.getUserAdmin(adminProfile, user.getId());
        Assert.assertEquals(user.getLogin(), testUser.getLogin());
        @Nullable UserDTO userDTO = null;
        try {
            userDTO = userClient.getUserAdmin(userProfile, user.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 403"));
        }
        Assert.assertNull(userDTO);
        userClient.deleteUserAdmin(adminProfile, user.getId());
    }

    @Test
    public void removeUserAdmin() {
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(UUID.randomUUID().toString());
        userClient.createUser(user);
        Assert.assertEquals(user.getLogin(), userClient.getUserAdmin(adminProfile, user.getId()).getLogin());
        userClient.deleteUserAdmin(adminProfile, user.getId());
        try {
            userClient.getUserAdmin(adminProfile, user.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
    }

    @Test
    public void mergeUserTest() {
        @NotNull UserDTO user = new UserDTO();
        user.setLogin("testU");
        user.setPassword("1");
        userClient.createUser(user);
        @NotNull UserDTO testUser = userClient.getUserAdmin(adminProfile, user.getId());
        testUser.setEmail("1@1.ru");
        userClient.updateUser(auth("testU", "1"), testUser);
        testUser = userClient.getUserAdmin(adminProfile, testUser.getId());
        Assert.assertEquals("1@1.ru", testUser.getEmail());
        userClient.deleteUserAdmin(adminProfile, testUser.getId());
    }

    @Test
    public void removeUserTest() {
        @NotNull UserDTO user = new UserDTO();
        user.setLogin("testU");
        user.setPassword("1");
        userClient.createUser(user);
        @NotNull UserDTO testUser = userClient.getUserAdmin(adminProfile, user.getId());
        testUser.setEmail("1@1.ru");
        userClient.updateUser(auth("testU", "1"), testUser);
        testUser = userClient.getUserAdmin(adminProfile, testUser.getId());
        Assert.assertEquals("1@1.ru", testUser.getEmail());
        userClient.deleteUser(auth("testU", "1"));
        @Nullable UserDTO result = null;
        try {
            result = userClient.getUserAdmin(adminProfile, user.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
        Assert.assertNull(result);
    }

}
