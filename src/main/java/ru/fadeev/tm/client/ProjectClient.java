package ru.fadeev.tm.client;

import feign.Feign;
import feign.Headers;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.fadeev.tm.dto.ProjectDTO;

import java.util.List;

@FeignClient
@Headers("Content-Type: application/json")
public interface ProjectClient {

    static ProjectClient client(@NotNull final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectClient.class, baseUrl);
    }

    @NotNull
    @GetMapping("api/projects")
    List<ProjectDTO> findAll(@RequestHeader("Authorization") String header);

    @PostMapping("api/project")
    ProjectDTO createProject(@RequestHeader("Authorization") String header, @NotNull ProjectDTO projectDTO);

    @GetMapping("api/project/{id}")
    ProjectDTO getProject(@RequestHeader("Authorization") String header, @PathVariable(name = "id") @NotNull String id);

    @DeleteMapping("api/project/{id}")
    void deleteProject(@RequestHeader("Authorization") String header, @PathVariable(name = "id") @NotNull String id);

    @PutMapping("api/project")
    ProjectDTO updateProject(@RequestHeader("Authorization") String header, final ProjectDTO projectDTO);

}