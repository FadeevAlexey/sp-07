package ru.fadeev.tm.config;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetailsService;

@TestConfiguration
public class TestConfig {

    @Bean
    UserDetailsService userDetailsTest(){
        return new SpringSecurityTestConfigBean();
    }

}
