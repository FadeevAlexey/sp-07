package ru.fadeev.tm.service;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.fadeev.tm.App;
import ru.fadeev.tm.api.service.IUserService;

import static ru.fadeev.tm.util.TestUtil.admin;
import static ru.fadeev.tm.util.TestUtil.user;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public abstract class AbstractServiceTest {

    @Autowired
    protected IUserService userService;

    @Before
    public void setup() {
        userService.persist(admin());
        userService.persist(user());
    }

}
