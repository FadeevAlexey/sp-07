package ru.fadeev.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.hibernate.annotations.Cache;
import ru.fadeev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "app_project")
@JsonIgnoreProperties(value = {"user", "tasks"})
public class Project extends AbstractEntity {

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    @OneToMany(
            mappedBy = "project",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    @Column(name = "start_date")
    private Date startDate;

    @Nullable
    @Column(name = "finish_date")
    private Date finishDate;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    @Column(name = "creation_time", updatable = false)
    private Date creationTime = new Date(System.currentTimeMillis());

}