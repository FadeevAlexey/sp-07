package ru.fadeev.tm.resources;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.SessionUser;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProjectRestController {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @GetMapping(value = "/api/projects")
    public ResponseEntity<List<ProjectDTO>> projectListGet(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        @NotNull final List<ProjectDTO> projects = projectService.findAll(userId).stream()
                .map(ProjectDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok(projects);
    }

    @PostMapping(value = "/api/project")
    public ResponseEntity<ProjectDTO> projectCreatePost(
            @NotNull final Authentication authentication,
            @RequestBody @Nullable final ProjectDTO projectDTO) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        if (projectDTO == null) return ResponseEntity.badRequest().build();
        projectDTO.setUserId(userId);
        @NotNull final Project project = projectService.convertToProject(projectDTO);
        projectService.persist(project);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/api/project/{id}")
    public ResponseEntity<ProjectDTO> projectRemoveGet(
            @NotNull final Authentication authentication,
            @PathVariable @Nullable final String id) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        if (id == null || id.isEmpty()) return ResponseEntity.badRequest().build();
        projectService.remove(userId, id);
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = "/api/project")
    public ResponseEntity<ProjectDTO> projectEditPost(
            @NotNull final Authentication authentication,
            @RequestBody @Nullable ProjectDTO projectDTO) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        if (projectDTO == null) return ResponseEntity.badRequest().build();
        @NotNull final Project project = projectService.convertToProject(projectDTO);
        projectService.merge(project);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/api/project/{id}")
    public ResponseEntity<ProjectDTO> projectViewGet(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        @Nullable final Project project = projectService.findOne(userId, id);
        if (project == null) return ResponseEntity.notFound().build();
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
        return ResponseEntity.ok(projectDTO);
    }

}
