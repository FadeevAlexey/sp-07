package ru.fadeev.tm.security;

import org.junit.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.fadeev.tm.util.TestUtil.userHttpBasic;

public class SecurityApi extends AbstractSecurityTest {

    @Test
    public void getUsersApi() throws Exception {
        mvc.perform(get("/api/users")
                .with(userHttpBasic(ADMIN)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"));
    }

    @Test
    public void unAuthApi() throws Exception {
        mvc.perform(get("/api/users"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void forbiddenApi() throws Exception {
        mvc.perform(get("/api/users")
                .with(userHttpBasic(USER)))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

}