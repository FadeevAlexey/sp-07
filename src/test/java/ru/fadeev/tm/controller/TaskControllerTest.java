package ru.fadeev.tm.controller;

import org.junit.Assert;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.web.servlet.ModelAndView;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.util.TestUtil;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TaskControllerTest extends AbstractController {

    @Autowired
    private ITaskService taskService;

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void tasksViewTest() throws Exception {
        mvc.perform(get("/task"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("task_list"))
                .andExpect(model().attributeExists("login", "tasks"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void taskCreateTestGet() throws Exception {
        mvc.perform(get("/task/create"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("task_create"))
                .andExpect(model().attributeExists("login", "projects"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void taskCreateTestPost() throws Exception {
        mvc.perform(post("/task/create").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(TestUtil.buildUrlEncodedFormEntity(
                        "name", "testName",
                        "description", "testDescription"
                )))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/task?login=Admin"));

    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void taskRemoveTest() throws Exception {
        final Task task = new Task();
        task.setUser(userService.findUserByLogin("Admin"));
        task.setName(UUID.randomUUID().toString());
        taskService.persist(task);
        mvc.perform(get("/task/remove/" + task.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/task"));
        Assert.assertNull(taskService.findOne(task.getId()));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void taskViewTest() throws Exception {
        final Task task = new Task();
        task.setUser(userService.findUserByLogin("Admin"));
        task.setName(UUID.randomUUID().toString());
        taskService.persist(task);
        ModelAndView modelAndView = mvc.perform(get("/task/view/" + task.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getModelAndView();
        TaskDTO taskDTO = (TaskDTO) modelAndView.getModel().get("task");
        Assert.assertEquals(task.getId(), taskDTO.getId());
        taskService.remove(taskDTO.getId());
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void taskEditGet() throws Exception {
        final Task task = new Task();
        task.setUser(userService.findUserByLogin("Admin"));
        task.setName(UUID.randomUUID().toString());
        taskService.persist(task);
        final ModelAndView modelAndView = mvc.perform(get("/task/edit/" + task.getId()))
                .andDo(print())
                .andExpect(view().name("task_edit"))
                .andExpect(status().isOk())
                .andReturn().getModelAndView();

        TaskDTO taskDTO = (TaskDTO) modelAndView.getModel().get("task");
        Assert.assertEquals(task.getId(), taskDTO.getId());
        taskService.remove(task.getId());
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsTest")
    public void taskEditPost() throws Exception {
        final Task task = new Task();
        task.setUser(userService.findUserByLogin("Admin"));
        task.setName(UUID.randomUUID().toString());
        mvc.perform(post("/task/create").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(TestUtil.buildUrlEncodedFormEntity(
                        "name", "testName",
                        "description", "testDescription"
                )))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/task?login=Admin"));
    }

}