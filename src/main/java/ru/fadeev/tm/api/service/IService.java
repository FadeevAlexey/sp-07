package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {

    @NotNull
    List<T> findAll();

    @Nullable
    T findOne(@Nullable String id);

    void remove(@Nullable String id);

    void persist(@Nullable T t);

    void merge(@Nullable T t);

}