package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.entity.Task;

import java.util.Collection;

public interface ITaskService extends IService<Task> {

    @NotNull
    Collection<Task> findAll(@Nullable String userId);

    void removeAll(@Nullable String userId);

    void removeAll();

    @Nullable
    Task findOne(@Nullable String userId, String id);

    void remove(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task convertToTask(@NotNull final TaskDTO taskDTO);

}