package ru.fadeev.tm.restcontroller;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.fadeev.tm.dto.TaskDTO;

public class TaskRestApiTest extends AbstractRestApiTest {

    @Test
    public void persistTaskTest() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("testUser");
        taskClient.createTask(userProfile, task);
        @NotNull final TaskDTO taskResult = taskClient.getTask(userProfile, task.getId());
        Assert.assertEquals("testUser", taskResult.getName());
        taskClient.deleteTask(userProfile, task.getId());
    }

    @Test
    public void findAllTaskTest() {
        final int startTest = taskClient.findAll(userProfile).size();
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull TaskDTO task2 = new TaskDTO();
        @NotNull TaskDTO task3 = new TaskDTO();
        taskClient.createTask(userProfile, task);
        taskClient.createTask(userProfile, task2);
        taskClient.createTask(userProfile, task3);
        final int finishTest = taskClient.findAll(userProfile).size();
        Assert.assertEquals(startTest, finishTest - 3);
        Assert.assertNotNull(taskClient.getTask(userProfile, task.getId()));
        @Nullable TaskDTO taskDTO = null;
        try {
            taskDTO = taskClient.getTask(adminProfile, task.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
        Assert.assertNull(taskDTO);
        taskClient.deleteTask(userProfile, task.getId());
        taskClient.deleteTask(userProfile, task2.getId());
        taskClient.deleteTask(userProfile, task3.getId());
    }

    @Test
    public void getOneTaskTest() {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("test");
        taskClient.createTask(userProfile, task);
        @NotNull final TaskDTO testTask = taskClient.getTask(userProfile, task.getId());
        Assert.assertEquals(task.getName(), testTask.getName());
        @Nullable TaskDTO taskDTO = null;
        try {
            taskDTO = taskClient.getTask(adminProfile, task.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
        Assert.assertNull(taskDTO);
        taskClient.deleteTask(userProfile, task.getId());
    }

    @Test
    public void mergeTaskTest() {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("test");
        taskClient.createTask(userProfile, task);
        @NotNull TaskDTO testTask = taskClient.getTask(userProfile, task.getId());
        testTask.setDescription("test description");
        taskClient.updateTask(userProfile, testTask);
        testTask = taskClient.getTask(userProfile, testTask.getId());
        Assert.assertEquals("test description", testTask.getDescription());
        @Nullable TaskDTO taskDTO = null;
        try {
            taskDTO = taskClient.getTask(adminProfile, task.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
        Assert.assertNull(taskDTO);
        taskClient.deleteTask(userProfile, task.getId());
    }

    @Test
    public void removeTaskTest() {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("test");
        taskClient.createTask(userProfile, task);
        Assert.assertEquals("test", taskClient.getTask(userProfile, task.getId()).getName());
        taskClient.deleteTask(userProfile, task.getId());
        try {
            taskClient.getTask(userProfile, task.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
    }

}
