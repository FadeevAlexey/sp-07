package ru.fadeev.tm.security;

import lombok.Getter;
import lombok.Setter;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.fadeev.tm.App;
import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.dto.UserDTO;

import static ru.fadeev.tm.util.TestUtil.admin;
import static ru.fadeev.tm.util.TestUtil.user;

@Getter
@Setter
@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public abstract class AbstractSecurityTest {

    protected final static UserDTO ADMIN = new UserDTO("Admin", "admin");

    protected final static UserDTO USER = new UserDTO("User", "user");

    @Autowired
    protected ITaskRepository taskRepository;

    @Autowired
    protected IUserService userService;


    @Autowired
    private WebApplicationContext context;

    protected MockMvc mvc;

    @Before
    public void setup() {
        userService.persist(admin());
        userService.persist(user());
        mvc = MockMvcBuilders.
                webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

}