package ru.fadeev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.dto.UserDTO;
import ru.fadeev.tm.entity.SessionUser;
import ru.fadeev.tm.entity.User;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UserController {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @PostMapping("/registration")
    public ModelAndView createUser(@NotNull final UserDTO userDTO) {
        if (userService.isLoginExist(userDTO.getLogin())) {
            ModelAndView modelAndView = new ModelAndView("/login");
            modelAndView.addObject("loginExist", "Login already exist, try again");
            return modelAndView;
        }
        @NotNull final User user = userService.convertToUser(userDTO);
        @NotNull final ModelAndView model = new ModelAndView("redirect:/");
        userService.persist(user);
        return model;
    }

    @GetMapping("/user/view")
    public ModelAndView viewUser(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final ModelAndView model = new ModelAndView("user_view");
        model.addObject("login", sessionUser.getUsername());
        model.addObject("projectsCount", projectService.findAll(sessionUser.getUserId()).size());
        model.addObject("tasksCount", taskService.findAll(sessionUser.getUserId()).size());
        return model;
    }

    @GetMapping("/user/edit")
    public ModelAndView editUser(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final ModelAndView model = new ModelAndView("user_edit");
        model.addObject("login", sessionUser.getUsername());
        return model;
    }

    @PostMapping("/user/edit")
    public ModelAndView editUserPost(
            @NotNull final Authentication authentication,
            @NotNull final UserDTO userDTO) {
        if (userService.isLoginExist(userDTO.getLogin())) {
            ModelAndView modelAndView = new ModelAndView("user_edit");
            modelAndView.addObject("loginExist", "Login already exist, try again");
            return modelAndView;
        }
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        userDTO.setId(sessionUser.getUserId());
        @NotNull final User user = userService.convertToUser(userDTO);
        userService.merge(user);
        @NotNull final ModelAndView model = new ModelAndView("redirect:/");
        return model;
    }

    @GetMapping(value = "/user/remove")
    public ModelAndView userRemoveGet(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        userService.remove(userId);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/logout");
        modelAndView.addObject("login", sessionUser.getUsername());
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/admin/users")
    public ModelAndView userListGet(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final List<UserDTO> users = userService.findAll()
                .stream()
                .map(UserDTO::new)
                .collect(Collectors.toList());
        @NotNull final ModelAndView model = new ModelAndView("user_list");
        model.addObject("login", sessionUser.getUsername());
        model.addObject("users", users);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping("/admin/view/{id}")
    public ModelAndView adminView(
            @NotNull final Authentication authentication,
            @PathVariable String id) {
        @Nullable User user = userService.findOne(id);
        if (user == null) return new ModelAndView("redirect:/");
        @NotNull final ModelAndView model = new ModelAndView("user_view");
        model.addObject("login", user.getLogin());
        model.addObject("projectsCount", projectService.findAll(id).size());
        model.addObject("tasksCount", taskService.findAll(id).size());
        return model;
    }

    @GetMapping("/admin/remove/{id}")
    public ModelAndView adminRemove(
            @NotNull final Authentication authentication,
            @PathVariable String id) {
        @NotNull final ModelAndView model = new ModelAndView("redirect:/admin/users");
        userService.remove(id);
        return model;
    }

}